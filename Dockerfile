FROM fedora:40

LABEL maintainer="mosuke5 <w.vamos603@gmail.com>"

ENV OCP_CLIENT_URL  https://mirror.openshift.com/pub/openshift-v4/clients/ocp/latest/openshift-client-linux.tar.gz

# Download, deploy oc/kubectl and remove unnecessary files
RUN curl ${OCP_CLIENT_URL} -o /tmp/openshift-client-linux.tar.gz \
      && tar xvzf /tmp/openshift-client-linux.tar.gz -C /tmp \
      && mv /tmp/oc /usr/local/bin/oc \
      && mv /tmp/kubectl /usr/local/bin/kubectl \
      && rm /tmp/openshift-client-linux.tar.gz /tmp/README.md

RUN dnf install -y \
      bind-utils \
      git \
      iputils \
      iproute \
      skopeo \
      podman \
      strace \
      vim \
      tcpdump \
      traceroute \
      mysql \
      {a,apache,dns,h,if,io,inno,my,pg_,power,tip,virt-}top \
      latencytop-tui \
      iptraf-ng \
      iptstate \
      goaccess \
      netsniff-ng \
      net-tools \
      procps-ng \
      redis \
      stress \
      stress-ng \
      sysstat \
      && rm -rf /var/cache/dnf/* \
      && dnf clean all
