## container image for application debug on kubernetes
Kubernetes上のアプリケーションをデバッグするために利用するコンテナイメージです。
Kubernetesのメリットを最大限に活かすためにはコンテナイメージは軽量であるほうがよく、アプリケーションイメージにはネットワークツールなどのデバッグに必要なツールを含まないことが多いです。そのため、障害時に調査用に利用できるコンテナイメージを作成しました。

個人的に下記あたりのツールを使いたくインストールしてあります。

- curl
- dig
- nslookup
- strace
- ping
- netstat
- mysql client
- mongodb client
- vim
- git
- skopeo
- podman
- openshift client (oc command)
- kubectl
